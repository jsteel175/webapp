<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-deep-grey w3-animate-left" style="z-index:3;width:200px;" id="mySidebar">
    
    <div class="w3-bar-block w3-padding">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-white w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    <a href="index.php" class="w3-bar-item w3-button w3-padding"><i class="fa fa-dashboard fa-fw"></i>  Dashboard</a>
    <a href="#" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Live Feed</a>
    <a href="history.php" class="w3-bar-item w3-button w3-padding"><i class="fa fa-history fa-fw"></i>  Data Logging</a>
	<a href="weather.php" class="w3-bar-item w3-button w3-padding"><i class="fa fa-cloud fa-fw"></i>  Weather</a>
    <a href="settings.php" class="w3-bar-item w3-button w3-padding"><i class="fa fa-sliders fa-fw"></i>  Settings</a>
    <a href="/cp/index.php" target="_blank" class="w3-bar-item w3-button w3-padding"><i class="fa fa-cogs fa-fw"></i>  Utilities</a>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>
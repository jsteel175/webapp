<?php

include('db_conn.php');

$sql = "SELECT ID, Temp FROM garden_db.recordings ORDER BY ID DESC LIMIT 1";

$result=mysqli_query($con,$sql);

// Close the connection
mysqli_close($con);

//build array for cols
$table = array();
$table['cols'] = array(
    //Labels for the chart, these represent the column titles
    array('id' => '', 'label' => 'Label', 'type' => 'string'),
    array('id' => '', 'label' => 'Value', 'type' => 'number')
    ); 
//build array for rows
$rows = array();
foreach($result as $row){
    $temp = array();
     
    //Values
    $temp[] = array('v' => "");
    $temp[] = array('v' => $row['Temp']); 
    $rows[] = array('c' => $temp);
    }
    
    $result->free();
 
$table['rows'] = $rows;
 
$jsonTable = json_encode($table, true);
echo $jsonTable;


?>


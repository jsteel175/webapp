<!DOCTYPE html>
<html>
<head>    
<title>iGarden History</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/stylesheet.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/charts.js"></script>
</head>



<body class="w3-light-grey">


 <?php 
include 'menu.php'; 
include 'header.php';
?>


<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <header class="w3-container"> </header>
      
    <!-- Conditions Sections -->
    <div class="w3-container">
        <h5><b>History</b></h5>
        <br>
    </div>
    
    <div class="w3-container" id="form">
        <label>Time Range:</label>
        <select name="time" id="time">
            <option value="1 HOUR">Hour</option>
            <option value="1 DAY">Day</option>
            <option value="1 WEEK">Week</option>
            <option value="1 MONTH">Month</option>
        </select>
        <input id="submit" type="button" value="Go">
        <br>    
    </div>
   
     
    <div class="w3-container"> 
     
        <div class="w3-row-padding w3-margin-bottom">
                 
             
             <!-- Chart 1 -->
            <div class="w3-half"> 
                <div id="chart1_div"></div>
                <div class="w3-container w3-orange w3-text-white w3-padding-tiny">
                <h6>Temperature</h6>
                </div>
            </div>
              
            <!-- Chart 2 -->
            <div class="w3-half">
                <div id="chart2_div"></div>
                <div class="w3-container w3-blue w3-text-white w3-padding-tiny">
                <h6>Humidity</h6>
                </div>
            </div>
    
             </div>
        
    </div>    
</div> 

<!-- End Page Content -->
</body>
</html>

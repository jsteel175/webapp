<!DOCTYPE html>
<html>
<head>    
<title>iGarden Dashboard</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/stylesheet.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/gauges.js"></script>


<?php
include 'menu.php';
include 'header.php';
?>

<body class="w3-light-grey">
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <header class="w3-container"></header>
  <!-- End Header -->
  
  <!-- Conditions Row -->
  <div class="w3-container">
        <h5><b>Current Conditions</b></h5>
  </div>
  <!-- End Conditions Row -->
  
   <!-- Air/Light Conditions-->
  <div>
  <div class="w3-container">
        <h5><b>Air/Light</b></h5>
  </div>
      
  
  <!-- Begin Charts -->  
  <div class="w3-threequarter">  
     
      <div class="w3-row-padding w3-margin-bottom">
            
        <!-- Chart 1 -->
            <div class= "w3-quarter">
			  
				<div id="chart1_div" align="center"></div>
				 	
              <div class="w3-container w3-orange w3-text-white w3-padding-tiny">
                <h6>Temperature</h6>
                </div>
            </div>
              
            <!-- Chart 2 -->
            <div class="w3-quarter" align="center">
                <div id="chart2_div"></div>
                <div class="w3-container w3-blue w3-text-white w3-padding-tiny">
                <h6>Humidity</h6>
                </div>
            </div>
    
            <!-- Chart 3 -->    
            <div class="w3-quarter">
                <div id="chart3_div" align="center"></div>
                <div class="w3-container w3-teal w3-text-white w3-padding-tiny">
                <h6>UV Level</h6>
                </div>
            </div>
               
        </div>
    </div>  
	</div>
  <!-- End Air/Light Conditions -- >
  
   <!-- Nutrient Conditions-->
  <div>
  <div class="w3-container">
        <h5><b>Nurients</b></h5>
  </div>
    
  
  <!-- Begin Charts -->  
  <div class="w3-threequarter">  
     
      <div class="w3-row-padding w3-margin-bottom">
            
        <!-- Chart 1 -->
            <div class= "w3-quarter">
			  	<div id="chart4_div" align="center"></div>
				<div class="w3-container w3-orange w3-text-white w3-padding-tiny">
                <h6>Temperature</h6>
                </div>
            </div>
              
            <!-- Chart 2 -->
            <div class="w3-quarter" align="center">
                <div id="chart5_div"></div>
                <div class="w3-container w3-blue w3-text-white w3-padding-tiny">
                <h6>pH Level</h6>
                </div>
            </div>
    
            <!-- Chart 3 -->    
            <div class="w3-quarter">
                <div id="chart6_div" align="center"></div>
                <div class="w3-container w3-teal w3-text-white w3-padding-tiny">
                <h6>EC Level</h6>
                </div>
            </div>
               
        </div>
    </div>  
	</div>
  <!-- End Nutrient Conditions -- >
  
  
  
  <hr>
  
    <!-- Equipment Section -->
      <div class="w3-container">
    
        
        
        <div class="w3-row-padding" style="margin:0 -16px">
      
            <div class="w3-half"><h5><b>Equipment Status</b></h5>
                <table class="w3-table w3-striped w3-white">
                    <tr>
                        <td><i class="fa fa-shower fa-fw"></i></td>
                        <td>Nutrient Pump Status:</td>
                        <td><input type="radio" name="np_status" value="on" checked> On</td>
                        <td><input type="radio" name="np_status" value="off"> Off</td>                       
                    </tr>
                    <tr>
                        <td><i class="fa fa-retweet fa-fw"></i></td>
                        <td>Oxygen Pump Status:</td>
                        <td><input type="radio" name="op_status" value="on" checked> On</td>
                        <td><input type="radio" name="op_status" value="off"> Off</td> 
                    </tr>
                    <tr>
                        <td><i class="fa fa-camera fa-fw"></i></td>
                        <td>Camera Status:</td>
                        <td><input type="radio" name="cam_status" value="on" checked> On</td>
                        <td><input type="radio" name="cam_status" value="off"> Off</td> 
                    </tr>
                    <tr>
                        <td><i class="fa fa-lightbulb-o fa-fw"></i></td>
                        <td>Light Status:</td>
                        <td><input type="radio" name="light_status" value="on"> On</td>
                        <td><input type="radio" name="light_status" value="off" checked> Off</td> 
                    </tr> 
                    
                </table>
            </div>
        </div>
   
    </div>
    <!-- End Equipment Section -->
    
    <hr>
    
    <!-- Pump Activity Section -->
     <div class="w3-container">
         
        <div class="w3-half">
            <h5><b>Nutrient Pump Activity</b></h5>
        </div> 
        
        <div class="w3-half">
            <h5><b>Oxygen Pump Activity</b></h5>
        </div> 
     </div>
    
</div>
<!-- End Page Content -->
</body>
</html>

// Load Charts and the corechart and barchart packages.
google.charts.load('current', {'packages':['corechart','gauge']});
   
// Draw the line charts when Charts is loaded.
google.charts.setOnLoadCallback(drawLineChart1);
google.charts.setOnLoadCallback(drawLineChart2);


     
    // Build the Temperature Chart
    function drawLineChart1(time) {
        if (time === undefined) {
          time = "1 WEEK";
    } 
        var dataString = "time=" + time;
        var jsonData = $.ajax({
        type: "GET",
        url: "get_hist_temp.php",
        data: dataString,
        dataType: "json",
        async: false
        }).responseText;
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(jsonData);
   
    // Set chart options here 
    var options = {
    legend: {position: 'none'},
    hAxis: {title: '', textPosition: 'none'},
    vAxis: {title: 'Temp (F)'}  
    };
      
    // Place chart in page and draw
    var chart = new google.visualization.LineChart(document.getElementById('chart1_div'));
    chart.draw(data, options);
    
    }
    
    
    // Build the Humidity Chart
    function drawLineChart2(time) {

        if (time === undefined) {
          time = "1 WEEK";
    } 
        var dataString = "time=" + time;
        var jsonData = $.ajax({
        type: "GET",
        url: "get_hist_humid.php",
        data: dataString,
        dataType: "json",
        async: false
        }).responseText;
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(jsonData);
        
    // Set chart options here 
    var options = {
    legend: {position: 'none'},
    hAxis: {title: '', textPosition: 'none'},
    vAxis: {title: 'Humidity'}
    };

      var chart = new google.visualization.LineChart(document.getElementById('chart2_div'));
        chart.draw(data, options);
     
     }
$(document).ready(function(){
    $("#submit").click(function(){
    var time = $("#time").val();
    drawLineChart1(time);
    drawLineChart2(time);
    return false;
    });
});
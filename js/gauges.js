 google.charts.load('current', {'packages':['gauge']});
 // Draw the guage charts when Charts is loaded.
    
	google.charts.setOnLoadCallback(drawGaugeChartAirTemp);
	google.charts.setOnLoadCallback(drawGaugeChartHumidity);
	google.charts.setOnLoadCallback(drawGaugeChartLight);
	google.charts.setOnLoadCallback(drawGaugeChartNutrientTemp);
    google.charts.setOnLoadCallback(drawGaugeChartpH);
	google.charts.setOnLoadCallback(drawGaugeChartEc);
    
       
   //Air Temperature Gauge Chart
	function drawGaugeChartAirTemp() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 115, height: 115,
            redFrom: 5.0, redTo: 6.0,
          yellowColor: '#DC3912',
          yellowFrom: 7.0, yellowTo: 8.0,
          min: 5.0, max: 8.0,
          minorTicks: 5
          };

        var chart = new google.visualization.Gauge(document.getElementById('gauge1_div'));

        chart.draw(data, options);

        setInterval(function() {
          //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_temp.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData); 
          chart.draw(data, options);
        }, 5000);
        
      }
	 
      
      //Humidity Chart
      function drawGaugeChartHumidity() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 115, height: 115,
            redFrom: 5.0, redTo: 6.0,
          yellowColor: '#DC3912',
          yellowFrom: 7.0, yellowTo: 8.0,
          min: 5.0, max: 8.0,
          minorTicks: 5
          };

        var chart = new google.visualization.Gauge(document.getElementById('gauge2_div'));

        chart.draw(data, options);

        setInterval(function() {
          //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_humid.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData); 
          chart.draw(data, options);
        }, 5000);
        
      }
      
      //UV-Light Gauge Chart
      function drawGaugeChartLight() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 115, height: 115,
          redFrom: 50, redTo: 65,
          yellowColor: '#DC3912',
          yellowFrom: 85, yellowTo: 100,
          min: 50, max:100,
          minorTicks: 5
          
        };

        var chart = new google.visualization.Gauge(document.getElementById('gauge3_div'));

        chart.draw(data, options);

        setInterval(function() {
          //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_humid.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData);   
          chart.draw(data, options);
        }, 5000);
        
      }
      
      
      //Gauge Chart 4
      function drawGaugeChartNutrientTemp() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 115, height: 115,
          redFrom: 0, redTo: 30,
          yellowColor: '#DC3912',
          yellowFrom: 70, yellowTo: 100,
          min: 0, max:100,
          minorTicks: 5
          
        };

        var chart = new google.visualization.Gauge(document.getElementById('gauge4_div'));

        chart.draw(data, options);

        setInterval(function() {
        
            //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_humid.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData);
          chart.draw(data, options);
        }, 5000);
        
      }
	  
	    //Gauge Chart 5
      function drawGaugeChartpH() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 150, height: 150,
          redFrom: 0, redTo: 30,
          yellowColor: '#DC3912',
          yellowFrom: 70, yellowTo: 100,
          min: 0, max:100,
          minorTicks: 5
          
        };

        var chart = new google.visualization.Gauge(document.getElementById('gauge5_div'));

        chart.draw(data, options);

        setInterval(function() {
        
            //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_humid.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData);
          chart.draw(data, options);
        }, 5000);
        
      }
	  
	   //Gauge Chart 6
      function drawGaugeChartEc() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', 0],
          ]);

        var options = {
          width: 150, height: 150,
          redFrom: 0, redTo: 30,
          yellowColor: '#DC3912',
          yellowFrom: 70, yellowTo: 100,
          min: 0, max:100,
          minorTicks: 5
          
        };

        var chart = new google.visualization.Gauge(document.getElementById('gauge6_div'));

        chart.draw(data, options);

        setInterval(function() {
        
            //get json table data from php page
        var jsonData = $.ajax({
        url: "get_current_humid.php",
        dataType: "json",
        async: false
        }).responseText;
        
        var data = new google.visualization.DataTable(jsonData);
          chart.draw(data, options);
        }, 5000);
        
      }
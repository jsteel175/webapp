<?php
include 'db_conn.php';


$timeInt =  $_GET["time"];

// if ( ! isset($timeInt) ){
//   $timeInt= '1 WEEK';
//  }

 
$sql = "SELECT Date, Temp FROM garden_db.recordings WHERE recordings.Date > DATE_SUB(NOW(), INTERVAL $timeInt)";

$result=mysqli_query($con,$sql);

// build array for cols
$table = array();
$table['cols'] = array(
    //Labels for the chart, these represent the column titles
    array('id' => '', 'label' => 'Date', 'type' => 'string'),
    array('id' => '', 'label' => 'Temp', 'type' => 'number')
    ); 
//build array for rows
$rows = array();
foreach($result as $row){
    $temp = array();
     
    //Values
    $temp[] = array('v' => $row['Date']);
    $temp[] = array('v' => $row['Temp']); 
    $rows[] = array('c' => $temp);
    }
    
    $result->free();
 
$table['rows'] = $rows;
 
$jsonTable = json_encode($table, true);
echo $jsonTable;

// Close the connection
mysqli_close($con);
?>
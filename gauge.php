<!DOCTYPE html>

<html>
  <head>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Temp', 80],
          ]);

        var TempOptions = {
          width: 400, height: 150,
          redFrom: 50, redTo: 65,
          yellowColor: '#DC3912',
          yellowFrom: 85, yellowTo: 100,
          min: 50, max:100,
          minorTicks: 5
          
        };

        var TempChart = new google.visualization.Gauge(document.getElementById('chart_div'));

        TempChart.draw(data, TempOptions);

        setInterval(function() {
          data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
          TempChart.draw(data, TempOptions);
        }, 5000);
        
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 500px; height: 200px;"></div>
  <?php
            
      ?>
  </body>
</html>

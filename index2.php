<!DOCTYPE html>
<html>
	<head>    
		<title>iGarden Dashboard</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="/css/stylesheet.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="/js/gauges.js"></script>
		<script type="text/javascript" src="/js/charts.js"></script>
	</head>

	<body class="w3-light-grey">

		<?php
		include 'menu.php';
		include 'header.php';
		?>

		<!-- !PAGE CONTENT! -->
		<div class="w3-main" style="margin-left:300px;margin-top:43px;">

		<!-- Begin Side Navbar -->
		<!-- End Side Navbar -->
		

		<!-- Begin Dashboard -->		
		<div id="dashboard" class="w3-container w3-twothird w3-row-padding">
		<!-- Dashboard Heading Row -->
			<div class="w3-row-padding">
				<h5><b>Dashboard</b></h5>
			</div>
		<!-- End Dashboard Heading Row -->	
		<!-- Begin Gauges -->  
			 
				<div class="w3-row-padding w3-margin-bottom">
					<!-- Chart 1 -->
					<div class= "w3-third">
						<div id="gauge1_div" align="center">
						</div>
						<div class="w3-container w3-orange w3-text-white w3-padding-tiny" align="center">
							<h6>Air Temp</h6>
						</div>
					</div>
              
					<!-- Chart 2 -->
					<div class="w3-third">
						<div id="gauge2_div" align="center">
						</div>
						<div class="w3-container w3-blue w3-text-white w3-padding-tiny">
							<h6>Humidity</h6>
						</div>
					</div>
    
					<!-- Chart 3 -->    
					<div class="w3-third">
						<div id="gauge3_div" align="center">
						</div>
						<div class="w3-container w3-teal w3-text-white w3-padding-tiny">
							<h6>UV Level</h6>
						</div>
					</div>
               
					<!-- Chart 4 -->
					<div class= "w3-third" align="center">
						<div id="gauge4_div">
						</div>
						<div class="w3-container w3-orange w3-text-white w3-padding-tiny">
							<h6>Nutrient Temp</h6>
						</div>
					</div>
              
					<!-- Chart 5 -->
					<div class="w3-third" align="center">
						<div id="gauge5_div">
						</div>
						<div class="w3-container w3-blue w3-text-white w3-padding-tiny">
							<h6>pH Level</h6>
						</div>
					</div>
    
					<!-- Chart 6 -->    
					<div class="w3-third">
						<div id="gauge6_div" align="center">
						</div>
						<div class="w3-container w3-teal w3-text-white w3-padding-tiny">
							<h6>EC Level</h6>
						</div>
					</div>
				</div>       
			
		<!-- End Gauges -->
		
		<!-- Charts Section -->
			    <div class="w3-container">
        <h5><b>History</b></h5>
        <br>
    </div>
    
    <div class="w3-container" id="form">
        <label>Time Range:</label>
        <select name="time" id="time">
            <option value="1 HOUR">Hour</option>
            <option value="1 DAY">Day</option>
            <option value="1 WEEK">Week</option>
            <option value="1 MONTH">Month</option>
        </select>
        <input id="submit" type="button" value="Go">
        <br>    
    </div>
   
     
    <div class="w3-container"> 
     
        <div class="w3-row-padding w3-margin-bottom">
                 
             
             <!-- Chart 1 -->
            <div class="w3-half"> 
                <div id="chart1_div"></div>
                <div class="w3-container w3-orange w3-text-white w3-padding-tiny">
                <h6>Temperature</h6>
                </div>
            </div>
              
            <!-- Chart 2 -->
            <div class="w3-half">
                <div id="chart2_div"></div>
                <div class="w3-container w3-blue w3-text-white w3-padding-tiny">
                <h6>Humidity</h6>
                </div>
            </div>
    
             </div>
        
    </div>
		<!-- End Charts Section -->
  
  
	
		<!-- Equipment Section -->
			<div class="w3-container">
				<div class="w3-row-padding" style="margin:0 -16px">
					<div class="w3-half">
						<h5><b>Equipment Status</b></h5>
						<table class="w3-table w3-striped w3-white">
						<tr>
							<td><i class="fa fa-shower fa-fw"></i></td>
							<td>Nutrient Pump Status:</td>
							<td><input type="radio" name="np_status" value="on" checked> On</td>
							<td><input type="radio" name="np_status" value="off"> Off</td>                       
						</tr>
						<tr>
							<td><i class="fa fa-retweet fa-fw"></i></td>
							<td>Oxygen Pump Status:</td>
							<td><input type="radio" name="op_status" value="on" checked> On</td>
							<td><input type="radio" name="op_status" value="off"> Off</td> 
						</tr>
						<tr>
							<td><i class="fa fa-camera fa-fw"></i></td>
							<td>Camera Status:</td>
							<td><input type="radio" name="cam_status" value="on" checked> On</td>
							<td><input type="radio" name="cam_status" value="off"> Off</td> 
						</tr>
						<tr>
							<td><i class="fa fa-lightbulb-o fa-fw"></i></td>
							<td>Light Status:</td>
							<td><input type="radio" name="light_status" value="on"> On</td>
							<td><input type="radio" name="light_status" value="off" checked> Off</td> 
						</tr> 
						</table>
					</div>
				</div>
	   
			</div>
			<!-- End Equipment Section -->
			
	 </div>

	 
    <!-- Forecast block -->
			<div class="w3-third">
				<div class="w3-row-padding w3-white w3-text-grey">
					<p>Party here!</p>
					<p>ipsum lorem frdoe cirvmfpx memeomcoecmcmo
					edkoekdoekdoe emeodmoemoepepep </p>
					<p>medodmodmedojeofeooem eomoemoemoe xoemoxmexomexomxeomxoexmoemw dcc oecmoecoem oemoemomexomoemxmxmoxmeome ececmcec 
					</p>
					<p>dedeod nxiwncwnci nnxnxnx cjrprenv weicneirnverp x enicnwecw wcpiwecopwecpwc  cencicicincic cienciwencnwecnewpccdncndc
					dc,ocmocoecnecpece mceoce oecpwcpwecpec ecpiecnecepnc </p>
				</div>
			</div>
		<!-- End Forecast Block -->
			
		</div>
	<!-- End Page Content -->
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>    
		<title>iGarden Dashboard</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="/css/stylesheet.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="/js/gauges.js"></script>
		<script type="text/javascript" src="/js/charts.js"></script>
		
	</head>

	<body class="w3-light-grey">

		<?php
		include 'menu.php';
		include 'header.php';
		?>

		<!-- !PAGE CONTENT! -->
		<div class="w3-main" style="margin-left:225px;margin-top:43px;">

			<!-- Begin Dashboard -->		
			<div id="dashboard" class="w3-container">
				
				<div class="w3-light-grey w3-container w3-border w3-round" style="margin-top:15px">
					
					<div class="w3-threequarter w3-padding w3-text-grey">
						<h4><b>Dashboard</b></h4>
					</div>
					
					<div id="weather" class="w3-quarter w3-right-align">
					   
					   <div class="w3-text-white">
				
									
								<div class="w3-weather-box w3-center-align">
								<h6>Englewood,FL</h6>
								</div>
									
								<div id="weather_temp" class="w3-weather-box w3-temp-text w3-half w3-right-align">
								</div>
								
								<div id="weather_icon" class="w3-weather-box w3-half w3-left-align">
								   <img id="icon">
								</div
								
								<div id="weather_conditions" class="w3-weather-box w3-center-align">
								 Cloudy and cool
								</div>
						</div>	
					
					
					
					
					</div>
				</div>
				
				<!-- Begin Panels -->
				<div id="panels">
						<div class="w3-panel w3-quarter">
							<div class="w3-container w3-border w3-round w3-orange">
								<div class="w3-half"> 
									<div id="gauge1_div"></div>
								</div>
								<div class="w3-half w3-text-white">	
									<h5>Temp</h5>
									<p><h1>75</h1></p>
								</div>
							</div>	
						</div>
					
						<div class="w3-panel w3-quarter">
							<div class="w3-container w3-border w3-round w3-green">
								<div class="w3-half"> 
									<div id="gauge2_div"></div>
								</div>
								<div class="w3-half w3-text-white">	
									<h5>pH Level</h5>
									<p><h1>70</h1></p>
								</div>
							</div>
						</div>
					
						<div class="w3-panel w3-quarter">
							<div class="w3-container w3-border w3-round w3-yellow">
								<div class="w3-half"> 
									<div id="gauge3_div"></div>
								</div>
								<div class="w3-half w3-text-white">	
									<h5>EC Level</h5>
									<p><h1>70</h1></p>
								</div>
							</div>			
						</div>
						
						<div class="w3-panel w3-quarter">
							<div class="w3-container w3-border w3-round w3-blue">
								<div class="w3-half"> 
									<div id="gauge4_div"></div>
								</div>
								<div class="w3-half w3-text-white">	
									<h5>UV Level</h5>
									<p><h1>70</h1></p>
								</div>
							</div>		
						</div>
				</div>
				
			

				<!-- Charts Section -->
					<div class="w3-container w3-threequarter w3-white w3-border w3-round">
							
							<div class="w3-text-grey">
								<h5><b>Real Time Data</b></h5>
							</div>
					
							<div id="form">
								<label>Select Data:</label>
								<select name="time" id="time">
									<option value="air_temp">Air Temp</option>
									<option value="humidity">Humidity</option>
									<option value="nutrient_temp">Nutrient Temp</option>
									<option value="nutrient_ph">Nutrient pH</option>
									<option value="nutrient_ec">Nutrient EC</option>
								</select>
								<input id="submit" type="button" value="Apply">
					 
							</div>		
							
   
					<!-- Chart 1 -->
						
							<div id="chart">
								<div id="chart1_div"></div>
								<div class="w3-text-dark-grey w3-padding-tiny">
									<h6>Ambient Air Temperature</h6>
								</div>    
							</div>
					</div> <!-- End Charts Section -->
				
				
					 <!-- Widgets block -->
				<div class="w3-container w3-quarter">
						<div class="w3-container w3-border w3-round">	
							<div class="w3-panel">
								<div class="w3-half w3-image"> 
									<img src="/images/avatar3.png" alt="sun" style="width:100%;max-width:80px">
								</div>
								<div class="w3-half w3-text-dark-grey">	
									Nutrient Level
									<p>100%</p>
								</div>
							</div>
							
							<div class="w3-panel">
								<div class="w3-half w3-image"> 
									<i class="fa fa-history fa-fw"></i>
								</div>
								<div class="w3-half w3-text-dark-grey">	
									UV Level
									<p>100</p>
								</div>
							</div>
						</div>
				
				
				</div>	<!-- End Widgets Block -->
			
			
			
			</div>	<!-- End Dashboard -->	
			
			
			
			
			
				
		</div>  <!-- End Page Content -->			
	
	</div>
	</body>
	<script src="/js/get_weather.js"></script>
	
</html>
